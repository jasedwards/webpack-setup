'use strict';

const program = require('commander');
const { PLUGINS, getPlugin } = require('./plugins');
const { LOADERS, getLoader } = require('./loaders');
const processPath = process.cwd();
const path = require('path');

module.exports = function () {
  let options = parseOptions();
  let config = getConfig();

  config.devServer = require(options.serveqa ? './devserver.qa.config' : './devserver.local.config');
  if (options.hook) {
    let hookjs = require(path.join(process.cwd(), options.hook));
    config = hookjs.processConfig(config);
  }
  return config;
}

function getConfig() {
  return {
    mode: 'development',

    context: processPath,

    entry: ['core-js/fn/promise', './index.js'],

    output: {
      filename: '[name].bundle.js',
      chunkFilename: '[name].bundle.js'
    },

    devtool: 'inline-source-map',

    module: {
      rules: [
        getLoader(LOADERS.jsHint),
        getLoader(LOADERS.ts),
        getLoader(LOADERS.babel),
        getLoader(LOADERS.html),
        getLoader(LOADERS.css),
        getLoader(LOADERS.less),
        getLoader(LOADERS.file)
      ]
    },

    plugins: [
      getPlugin(PLUGINS.HtmlWebPackPlugin),
      getPlugin(PLUGINS.MiniCssExtractPlugin)
    ]
  };
}

function parseOptions() {
  program.option('--serveqa')
    .option('--hook [value]')
    .option('--open')
    .option('--progress')
    .option('--color')
    .option('--config')
    .option('--watch')
    .parse(process.argv);
  let serveqa = !!program.serveqa;
  let hook = program.hook
  return { serveqa, hook };
}