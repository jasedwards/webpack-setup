
  Usage: webpack-setup [options]

  webpack-setup provides command line options for required webpack setup in production/development/testing mode with default configuration which godd enough for most of scenarios. It also provide interface to override default configuration.

  Options:

    -V, --version                 output the version number
    -T, --task [type]             Run webpack task build/serve/test/promote/setpath
    -M, --mode [type]             Run webpack in prod/dev mode
    -O, --open [type]             Open browser in dev mode
    -Q, --qa                      Run webpack dev server with QA/local setup
    -A, --analyze                 Run analyzer to review package structure
    -H --hook [value]             Provide JS file name which will be hooked in the webpack execution process to allowed custom changes
    -W, --watch                   Watch for changes
    -P, --promoteVersion [value]  Promote Changes with options major/minor/path
    --appName [value]             App name to be used for deployment
    --appSrc [value]              Source folder path to be deployed
    --branch [value]              Source branch to be considered for deployment
    --zipTarget [value]           Path where zipped deployment package to created
    --enterprisePath [value]      Path of enterprise folder
    -h, --help                    output usage information

    TODO: Add documentation for commands
