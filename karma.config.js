// Reference: http://karma-runner.github.io/0.12/config/configuration-file.html
// adding this comment to test trigger in team city.  try again
const path = require('path');
const webpackConfig = require('./webpack.test.js');
const processPath = process.cwd();
const program = require('commander');

program.option('--hook [value]').option('--auto-watch').option('--no-single-run').parse(process.argv);
let hook = program.hook

module.exports = function karmaConfig(config) {
    let karmsConfig = getDefaultConfig();
    if (hook) {
        let hookjs = require(path.join(process.cwd(), hook));
        karmsConfig = hookjs.processConfig(karmsConfig);
    }
    config.set(karmsConfig);
};

function getDefaultConfig() {
    return {
        basePath: processPath,
        singleRun: true,
        autoWatch: false,
        browserNoActivityTimeout: 4 * 60 * 1000,
        browserDisconnectTimeout: 60000,
        captureTimeout: 4 * 60 * 1000,
        browserDisconnectTolerance: 3,
        frameworks: [
            'jasmine'
        ],
        reporters: [
            'progress',
            'coverage',
            'teamcity'
        ],
        files: [
            'test.webpack.js'
        ],
        preprocessors: {
            'test.webpack.js': ['webpack', 'sourcemap']
        },
        customLaunchers: {
            FirefoxHeadless: {
                base: "Firefox",
                flags: ["-headless"]
            }
        },
        browsers: [
            'FirefoxHeadless'
        ],
        coverageReporter: {
            dir: 'coverage/',
            reporters: [
                { type: 'text-summary' },
                { type: 'html' }
            ]
        },
        webpack: webpackConfig,
        webpackMiddleware: {
            noInfo: 'errors-only'
        }
    }
}
