const path = require('path');
const fs = require('fs');

module.exports = { setPath };

function setPath(enterprisePath) {
    if (!enterprisePath) {
        throw 'could not find a value for path. Should be in the form of --path=c:/code...';
    }
    const projectConfigPath = path.join(process.cwd(), 'projectConfig.json');
    fs.writeFileSync(projectConfigPath, `{"enterpriseDirectory":"${enterprisePath}"}`);
}