
module.exports = {
  contentBase: './src/public',
  stats: 'minimal',
  proxy: {
    "/StarsOne/*": {
      "context": "/enterprise/",
      "host": "localhost",
      "target": "https://trunk.riskonnectclearsight.com",
      "port": 80,
      "secure": false,
      "changeOrigin": true,
      "pathRewrite": {
        "^/StarsOne": "/enterprise"
      },
      "headers": {
        "Stars-Authentication": "E321TEST_TRUNK:StarsAdmin:Stars@5me:MCT"
      }
    },
    "/nodejs/*": {
      "host": "localhost",
      "context": "/enterprise/nodejs/",
      "target": "https://trunk.riskonnectclearsight.com",
      "port": 80,
      "secure": false,
      "changeOrigin": true,
      "xforward": false
    }
  }
};
