
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const { PLUGINS, getPlugin } = require('./plugins');

const processPath = process.cwd();
const LOADERS = {
    jsHint: 'jshint-loader', ts: 'ts-loader', babel: 'babel-loader',
    html: 'html-loader', css: 'css-loader',
    less: 'less-loader', file: 'file-loader',
    annotate: 'annotate-loader', instrumenter: 'instrumenter-loader'
};
const cssLoader = { loader: "css-loader", options: { minimize: true } }
const lessLoader = {
    loader: "less-loader",
    options: {
        minimize: true,
        plugins: [getPlugin(PLUGINS.LessGlobPlugin)]
    }
}

module.exports = { getLoader, LOADERS };

function getLoader(name, mode) {
    switch (name) {
        case LOADERS.jsHint:
            return {
                test: /src\\.*\.js$/, enforce: 'pre',
                exclude: /node_modules|bower_components|^\.temp|\.bower-tmp|^coverage$/,
                use: [{ loader: `jshint-loader` }]
            };
        case LOADERS.ts:
            return { test: /\.tsx?$/, use: 'ts-loader', exclude: /node_modules/ };
        case LOADERS.babel:
            return { test: /\.js$/, exclude: /node_modules\\(?!@clearsight).*/, use: { loader: "babel-loader" } };
        case LOADERS.html:
            return {
                test: /\.html$/, use: [{ loader: "html-loader", options: { minimize: true } }]
            };
        case LOADERS.css:
            return { test: /\.css$/, use: [MiniCssExtractPlugin.loader, cssLoader] };
        case LOADERS.less:
            return { test: /\.less$/, use: [MiniCssExtractPlugin.loader, cssLoader, lessLoader] };
        case LOADERS.file:
            return {
                test: /\.(png|jpg|jpeg|gif|svg|woff|woff2|ttf|eot|ico)$/, loader: 'file-loader', options: { url: false }
            };
        case LOADERS.annotate:
            return {
                test: /src.*\.js$/,
                exclude: [
                    /node_modules\\(?!@clearsight).*/,
                    /\.spec\.js$/
                ],
                use: [{ loader: 'ng-annotate-loader', options: { map: false } }],
            };
        case LOADERS.instrumenter:
            return {
                enforce: 'pre',
                test: /\.js$/,
                exclude: [
                    /node_modules/,
                    /\.spec\.js$/
                ],
                loader: 'istanbul-instrumenter-loader',
                query: {
                    esModules: true
                }
            }
    }

    throw `Webpack loader "${name}" not found!!`;
}