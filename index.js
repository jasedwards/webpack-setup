
module.exports = webpack_setup;

const path = require('path');
const exec = require('child_process').exec;
const cmdBuilder = require('./command-builder');

function webpack_setup(options) {

  var cmd = cmdBuilder.buildCommand(options);
  if (typeof cmd === 'function') {
    cmd();
  } else {
    console.log(`command: ${cmd}`);
    var outRef = exec(cmd, { maxBuffer: 2000 * 1024 },
      function (err, stdout, stderr) {
        if (err) {
          console.log(err);
          process.exit(1);
        }
      });
    outRef.stdout.pipe(process.stdout);
    outRef.stderr.pipe(process.stderr);
  }
}
