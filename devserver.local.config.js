
module.exports = {
    contentBase: './src/public',
    stats: 'minimal',
    proxy: {
        "/StarsOne/*": {
            "context": "/Clearsight/",
            "host": "localhost",
            "target": "http://localhost",
            "port": 80,
            "secure": false,
            "changeOrigin": true,
            "pathRewrite": { "^/StarsOne": "/Clearsight" },
            "headers": { "Stars-Authentication": "RMIS_MULTICLIENT:OWNER:OWNER:ABC" }
        }
    }
};
