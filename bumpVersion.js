const fs = require('fs');
const path = require('path');

function increaseVersion(parts, index){
    let num = parseInt(parts[index]);
    num = num + 1;
    parts[index] = num;
    if (index < 2) {
        for (let i = index + 1; i < 3; i++) {
            parts[i] = 0;
        }
    }
    return parts;
}

function getDir(){
    return path.join(process.cwd(), 'package.json');
}

function readPackageJson(){
    try{
        const dir = getDir();
        fs.accessSync(dir, fs.constants.R_OK | fs.constants.W_OK);
        return JSON.parse(fs.readFileSync(dir));
    }catch(e){
        console.log('Unable to find or access package.json');
        process.exit(1);
    }
}

module.exports = function bumpVersion(index){
    const pkg = readPackageJson();
    pkg.version = increaseVersion(pkg.version.split('.'), index).join('.');
    fs.writeFileSync(getDir(), JSON.stringify(pkg, null, 4));
    return {version: pkg.version, name: pkg.name};
};