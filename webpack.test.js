'use strict';

const { LOADERS, getLoader } = require('./loaders');

module.exports = {
    mode: 'development',
    entry: void 0,
    output: {},
    devtool: 'inline-source-map',
    module: {
        rules: [
            getLoader(LOADERS.ts),
            getLoader(LOADERS.babel),
            getLoader(LOADERS.css),
            getLoader(LOADERS.less),
            getLoader(LOADERS.html),
            getLoader(LOADERS.file),
            getLoader(LOADERS.instrumenter)
        ]
    },
};
